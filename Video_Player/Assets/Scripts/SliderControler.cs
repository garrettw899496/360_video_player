﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

    /*  used to let the application know when the user is clicking on the timeline to prevent
        the timeline from being updated by the current frame when the user is trying to scrub the video
    */


public class SliderControler : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public Slider slider;
    public bool sliderHeld;


    public void OnPointerDown(PointerEventData data)
        {
            sliderHeld = true;
        }

    public void OnPointerUp(PointerEventData data)
        {
            sliderHeld = false;
        }

}   

