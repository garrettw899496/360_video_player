﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;




public class MenuController : MonoBehaviour
{
    public URLValueController newURLValueController;        //reference to the URL controller to hold the path value between scences 

    public void PlayVideo (string URL){          //sets the path for the selected video and changes scenes to the video player

        newURLValueController.SetURL(URL);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex +1);
        Debug.Log("test");

    }

    public void Exit() {
     Application.Quit();
 }

}
