﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Video;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlaybackController : MonoBehaviour
{
 
 public VideoPlayer videoPlayer;            
 public AudioSource audioSource;
 public Slider timeLine;                    
 public SliderControler sliderControler;    //used to know if the slider is being interacted with

 public URLValueController newURLValueController;   //holds the static value for the path of the video between scenes 


 public Button playPauseButton;
 public Button volumeButton;
 public Sprite playSprite;
 public Sprite pauseSprite;
 public Sprite muteSprite;
 public Sprite unMuteSprite;
 public Image initialPlayButton;

 
 bool timeLineIsUpdating = false;    //used to improve consistinsy of clicking on timeline to scrub through the video
 bool hasPlayed = false;

 

 void Awake(){                              //Loads and prepares the selected video when the video player is loaded
     videoPlayer.url = (Application.streamingAssetsPath + "/" + newURLValueController.GetURL());
     videoPlayer.Prepare();
 }


/*public getters for the VideoPlayer object type*/


 public bool isPlaying{
     get {return videoPlayer.isPlaying;}
 }

 public bool isPrepared{                    //set to true when video can be played
     get{return videoPlayer.isPrepared;}
 }


  public ulong duration{                    //takes the total frames of the video divided by the framerate to get the duration of the video 
     get {return (ulong)(videoPlayer.frameCount / videoPlayer.frameRate);}
 }

 public float time {                        //current time of the video between 0 and 1 to be used by the timeline
     get {return ((float)(videoPlayer.frame) / (float)(videoPlayer.frameCount));}
 }



/*public functions for the video player*/

private void Update(){                      //part of MonoBehaviour, updates on every frame update
    if(!sliderControler.sliderHeld && !timeLineIsUpdating)
        timeLine.normalizedValue = time;    //updates the timeline of the video with each frame change unless the timeline is still updating from a scrub
                                            //or the user is clicking on the timeline
    else{ 
        if(hasPlayed){                      //scrubs through video but only if video has started playing and wont allow the timeline to change again
                                            //until after it is updated 
            timeLineIsUpdating=true;
            videoPlayer.time = timeLine.normalizedValue * duration;
            timeLineIsUpdating = false;
        }

    }
}


void Play(){                                //plays the video, changes the sprite to the pause icon and removes
     if(!isPrepared) return;                //the center play image but will do nothing if the video isn't prepared 
     videoPlayer.Play();
     playPauseButton.image.sprite = pauseSprite;
     hasPlayed=true;
     initialPlayButton.enabled = false;
    
     
 }

void Pause(){                               //puases the video, changes the sprite to the play icon, and re-enables the center play image
     videoPlayer.Pause();
     playPauseButton.image.sprite = playSprite;
     initialPlayButton.enabled = true;
 }

public void PlayPause(){                    //used by the play button and screen click to play or pause the video

    if(!isPlaying){
        Play();
    }
    else{
        Pause();
    }
}


 public void Mute(){                        //mutes the video and changes the icon of the volume button
     if(audioSource.volume!=0.0f){
         audioSource.volume=0.0f;
         volumeButton.image.sprite = muteSprite;
     }
     else{
        audioSource.volume=1.0f;
        volumeButton.image.sprite = unMuteSprite;
     }
 }

 public void ReturnToMenu(){                //returns to the application menu when the back button is clicked 
     SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex -1);
 }

}
