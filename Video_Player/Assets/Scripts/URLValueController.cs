﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/* a script using a static value to hold the path to the selected video between scenes */

public class URLValueController : MonoBehaviour
{
   static string URL = "ERROR";

   public void SetURL(string tempURL){
       URL = tempURL;
    }

   public string GetURL(){ 
       return URL;
    }
        
}
